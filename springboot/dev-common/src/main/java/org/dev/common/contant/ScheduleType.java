package org.dev.common.contant;

public interface ScheduleType {
    String CRON = "CRON";
    String SIMPLE = "SIMPLE";
}

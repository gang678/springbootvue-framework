package org.dev.auth.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.auth.module.entity.SysFunc;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysFuncService extends IService<SysFunc> {

}

package org.dev.auth.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.auth.module.entity.SysUser;

/**
 * <p>
 * 用户基本信息 Mapper 接口
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}

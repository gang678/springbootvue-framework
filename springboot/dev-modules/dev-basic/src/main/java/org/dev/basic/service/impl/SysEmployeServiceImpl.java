package org.dev.basic.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dev.basic.entity.SysEmploye;
import org.dev.basic.mapper.SysEmployeMapper;
import org.dev.basic.service.SysEmployeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-08-02
 */
@Service
public class SysEmployeServiceImpl extends ServiceImpl<SysEmployeMapper, SysEmploye> implements SysEmployeService {

}

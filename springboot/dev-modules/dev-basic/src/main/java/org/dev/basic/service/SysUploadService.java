package org.dev.basic.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.basic.entity.SysUpload;

/**
 * <p>
 * 上传文件 服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-18
 */
public interface SysUploadService extends IService<SysUpload> {

}

package org.dev.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.basic.entity.SysTenant;

/**
 * <p>
 * 租户信息 Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-05
 */
public interface SysTenantMapper extends BaseMapper<SysTenant> {

}

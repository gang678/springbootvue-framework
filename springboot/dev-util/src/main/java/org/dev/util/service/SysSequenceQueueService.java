package org.dev.util.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.util.entity.SysSequenceQueue;

/**
 * <p>
 * 生成的队列号 服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-20
 */
public interface SysSequenceQueueService extends IService<SysSequenceQueue> {

}

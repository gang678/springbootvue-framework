package org.dev.util.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.util.entity.SysSequenceLog;

/**
 * <p>
 * 生成的队列号 服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-20
 */
public interface SysSequenceLogService extends IService<SysSequenceLog> {

}

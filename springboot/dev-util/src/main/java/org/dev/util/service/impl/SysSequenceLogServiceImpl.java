package org.dev.util.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dev.util.entity.SysSequenceLog;
import org.dev.util.mapper.SysSequenceLogMapper;
import org.dev.util.service.SysSequenceLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 生成的队列号 服务实现类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-20
 */
@Service
public class SysSequenceLogServiceImpl extends ServiceImpl<SysSequenceLogMapper, SysSequenceLog> implements SysSequenceLogService {

}
